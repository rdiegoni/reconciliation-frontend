const path = require('path');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = function override(config) {
    config.resolve.plugins.push(new TsconfigPathsPlugin({ 
        configFile: path.resolve(__dirname, "tsconfig.json"),
        extensions: ['.ts', '.tsx', '.js'],
    }))

    return config;
};