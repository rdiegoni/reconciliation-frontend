import { useRoutes, Navigate } from "react-router-dom";
import { styled } from '@mui/system';
import { CssBaseline } from '@externals/mui'
import { AccountsPage } from '@pages/AccountsPage/AccountsPage'
import { AccountPage } from '@pages/AccountPage/AccountPage'
import { Header } from '@components/atoms/Header/Header'

const Main = styled('main')(({ theme }) => ({
  padding: theme.spacing(5)
}))

const App = () => {
  const routes = useRoutes([
    {
      path: '/',
      element: <Navigate to='/accounts' replace />,
    },
    { 
      path: '/accounts', 
      element: <AccountsPage />,
    },
    { 
      path: '/accounts/:accountId', 
      element: <AccountPage />,
    }
  ])

  return (
    <div>
      <CssBaseline />
      <Header />
      <Main>{routes}</Main>
    </div>
  );
}

export default App;
