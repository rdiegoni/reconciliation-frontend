import { useState, useEffect } from "react";
import { getAccountsUrl } from "@utils/url";
import { convert } from "@utils/account";
import { Account } from "@models/account";

export const useAccounts = (): [accounts: Account[]] => {
  const [accounts, setAccounts] = useState<Account[]>([]);

  useEffect(() => {
    const fetchAccounts = async () => {
      const response = await fetch(getAccountsUrl(), {
        method: "GET",
        mode: "cors",
      });

      const data = await response.json();

      const currentAccounts = data.map(convert);
      setAccounts(currentAccounts);
    };

    fetchAccounts();
  }, []);

  return [accounts];
};
