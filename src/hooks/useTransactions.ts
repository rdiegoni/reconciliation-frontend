import { useState, useEffect } from "react";
import { getTransactionsUrl } from "@utils/url";
import { convert } from "@utils/transaction";
import { Transaction } from "@models/transaction";

export const useTransactions = (
  accountId: string | undefined
): [transactions: Transaction[]] => {
  const [transactions, setTransactions] = useState<Transaction[]>([]);

  useEffect(() => {
    const fetchTransactions = async () => {
      const response = await fetch(getTransactionsUrl(accountId), {
        method: "GET",
        mode: "cors",
      });

      const data = await response.json();

      const currentTransactions = data.map(convert);
      setTransactions(currentTransactions);
    };

    fetchTransactions();
  }, [accountId]);

  return [transactions];
};
