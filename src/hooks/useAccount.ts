import { useState, useEffect } from "react";
import { getAccountUrl } from "@utils/url";
import { convert } from "@utils/account";
import { Account } from "@models/account";

export const useAccount = (
  accountId: string | undefined
): [account: Account] => {
  const [account, setAccount] = useState<Account | any>({});

  useEffect(() => {
    const fetchAccounts = async () => {
      const response = await fetch(getAccountUrl(accountId), {
        method: "GET",
        mode: "cors",
      });

      const data = await response.json();
      setAccount(convert(data));
    };

    fetchAccounts();
  }, [accountId]);

  return [account];
};
