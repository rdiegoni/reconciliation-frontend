import { useParams } from 'react-router-dom'
import { AccountDetails } from '@components/molecules'
import { AccountTransactions } from '@components/organigrams'
import { useAccount } from '@hooks/useAccount'
import { useTransactions } from '@hooks/useTransactions'
import { Root } from './AccountPage.styles'

export const AccountPage = () => {
    const parameters = useParams()

    const [ account ] = useAccount(parameters.accountId)
    const [ transactions ] = useTransactions(parameters.accountId)
   
    return (
        <Root>
            <AccountDetails name={account.name} balances={account.balances}/>
            <AccountTransactions transactions={transactions} />
        </Root>
    )
}