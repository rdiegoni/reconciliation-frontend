import { styled } from '@mui/system';

export const Root = styled('div')(({ theme }) => ({
    padding: theme.spacing(4),
    '& > *:first-child': {
        marginBottom: theme.spacing(3)
    }
}))