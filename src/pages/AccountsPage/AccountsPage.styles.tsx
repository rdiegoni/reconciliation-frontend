import { styled } from '@mui/system';

export const Accounts = styled('div')(({ theme }) => ({
    padding: theme.spacing(4),
    '& > .MuiCard-root:not(:first-child)': {
        marginTop: theme.spacing(3)
    }
}))