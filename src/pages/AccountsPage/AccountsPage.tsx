import { AccountCard } from '@components/organigrams'
import { useAccounts } from '@hooks/useAccounts'
import { Account } from '@models/account'
import { Accounts } from './AccountsPage.styles'

export const AccountsPage = () => {
    const [accounts] = useAccounts()

    const renderAccount = (account: Account) => {
        return <AccountCard key={account.id} account={account} />
    }

    return (
        <Accounts>{accounts.map(renderAccount)}</Accounts>
    )
}