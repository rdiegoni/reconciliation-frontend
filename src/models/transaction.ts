export interface TransactionDto {
  id: string;
  accountId: string;
  amount: number;
  currency: string;
  type: string;
  date: string;
}

export type TransactionType = "Settled" | "Chargeback" | "Refunded";

export interface Transaction {
  id: string;
  accountId: string;
  amount: number;
  currency: string;
  type: TransactionType;
  date: Date;
}
