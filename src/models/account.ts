export interface BalanceDto {
  currency: string;
  amount: number;
}

export interface AccountDto {
  id: string;
  name: string;
  balances: BalanceDto[];
}

export interface Balance {
  currency: string;
  amount: number;
}

export interface Account {
  id: string;
  name: string;
  balances: Balance[];
}
