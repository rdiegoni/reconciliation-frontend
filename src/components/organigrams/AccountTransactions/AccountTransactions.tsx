import { useState } from 'react';
import { MUITable, MUITableBody, MUITableContainer, MUITableHead, MUITablePagination, MUIPaper } from '@externals/mui'
import { Amount } from '@components/atoms'
import { Balances } from '@components/molecules'
import { Transaction } from '@models/transaction'
import { getBalances } from '@utils/transaction'
import { formatDate } from '@utils/date'
import * as styles from './AccountTransactions.styles'

interface AccountTransactionsProps {
    transactions: Transaction[]
}

export const AccountTransactions = (props: AccountTransactionsProps) => {
    const { transactions } = props
    const { TableRow, TableCell } = styles

    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [page, setPage] = useState(0);

    const isSettled = (transaction: Transaction) => transaction.type === "Settled"

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const renderBody = () => {
        return currentTransactions
            .map((transaction: Transaction, index: number) => {
                const balances = getBalances(transactions, (page * rowsPerPage) + index + 1)
                const Component = <Amount amount={transaction.amount} currency={transaction.currency} />
                return (
                    <TableRow
                        key={transaction.id}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                        <TableCell>{formatDate(transaction.date)}</TableCell>
                        <TableCell align="right">{transaction.type}</TableCell>
                        <TableCell align="right">{isSettled(transaction) && Component}</TableCell>
                        <TableCell align="right">{!isSettled(transaction) && Component}</TableCell>
                        <TableCell align="right"><Balances balances={balances} /></TableCell>
                    </TableRow>
                )
            })
    }

    const renderHeaders = () => {
        return (
            <TableRow>
                <TableCell>Date</TableCell>
                <TableCell align="right">Type</TableCell>
                <TableCell align="right">Amount in</TableCell>
                <TableCell align="right">Amount out</TableCell>
                <TableCell align="right">Balances</TableCell>
            </TableRow>
        )
    }

    const currentTransactions = transactions.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
    return (
        <>
            <MUITableContainer component={MUIPaper}>
                <MUITable sx={{ minWidth: 650 }}>
                    <MUITableHead>
                        {renderHeaders()}
                    </MUITableHead>
                    <MUITableBody>
                        {renderBody()}
                    </MUITableBody>
                </MUITable>
            </MUITableContainer>
            <MUITablePagination
                rowsPerPageOptions={[10, 20, 30]}
                component="div"
                count={transactions.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </>
    )
}