import { Link } from "react-router-dom";
import { MUICard, MUICardActionArea, MUICardContent } from '@externals/mui'
import { AccountCardDetails } from '@components/molecules'
import { Account } from '@models/account'

interface AccountCardProps {
    account: Account
}

export const AccountCard = (props: AccountCardProps) => {
    const { account } = props
    return (
        <MUICard variant='outlined' square>
            <MUICardActionArea component={Link} to={`/accounts/${account.id}`}>
                <MUICardContent>
                    <AccountCardDetails name={account.name} balances={account.balances} />
                </MUICardContent>
            </MUICardActionArea>
        </MUICard>
    )
}