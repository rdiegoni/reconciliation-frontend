import { styled } from '@mui/system';
import { MUITypography } from '@externals/mui'

export const Root = styled('div')({
    display: 'flex',
    alignItems: 'center'
})

export const Label = styled(MUITypography)({
    flexGrow: 1
})
