import { Balances } from '@components/molecules'
import { Balance } from '@models/account'
import * as styles from './AccountCardDetails.styles'

interface AccountCardDetailsProps {
    name: string
    balances: Balance[]
}

export const AccountCardDetails = (props: AccountCardDetailsProps) => {
    const { name, balances } = props
    const { Root, Label } = styles
    return (
        <Root>
            <Label variant='body1'>{name}</Label>
            <Balances balances={balances} />
        </Root>
    )
}