import { Balances } from '@components/molecules'
import { Balance } from '@models/account'
import * as styles from './AccountDetails.styles'

interface AccountDetailsProps {
    name: string
    balances: Balance[]
}

export const AccountDetails = (props: AccountDetailsProps) => {
    const { name, balances = [] } = props
    const { Root, Vertical, NameLabel, Name, BalanceLabel } = styles
    return (
        <Root variant='outlined' square>
            <Vertical flexGrow={1}>
                <NameLabel>Name</NameLabel>
                <Name variant='body1'>{name}</Name>
            </Vertical>
            <Vertical>
                <BalanceLabel>Balances</BalanceLabel>
                <Balances balances={balances} />
            </Vertical>
        </Root>
    )
}