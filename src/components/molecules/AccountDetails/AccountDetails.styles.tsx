import { styled } from '@mui/system';
import { MUITypography, MUIPaper } from '@externals/mui'

export const Root = styled(MUIPaper)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(2)
}))

export const Vertical = styled('div', { shouldForwardProp: false })(({ flexGrow }) => ({
    display: 'flex',
    flexDirection: 'column',
    flexGrow: flexGrow
}))

export const NameLabel = styled(MUITypography)({
    fontWeight: 'bold'
})

export const Name = styled(MUITypography)({
})

export const BalanceLabel = styled(MUITypography)({
    fontWeight: 'bold'
})

