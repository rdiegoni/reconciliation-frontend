import { styled } from '@mui/system';

export const Root = styled('div')({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end'
})