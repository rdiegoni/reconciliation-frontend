import { Amount } from '@components/atoms'
import { Balance } from '@models/account'
import * as styles from './Balances.styles'

interface BalancesProps {
    balances: Balance[]
}

export const Balances = (props: BalancesProps) => {
    const { balances = [] } = props
    const { Root } = styles

    const renderBalance = (balance: Balance) => {
        return <Amount key={balance.currency} amount={balance.amount} currency={balance.currency} />
    }

    return (
        <Root>
            {balances.map(renderBalance)}
        </Root>
    )
}