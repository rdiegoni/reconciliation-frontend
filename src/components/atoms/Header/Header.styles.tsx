import { Link as RDLink } from "react-router-dom";
import { styled } from '@mui/system';

export const Root = styled('header')(({ theme }) => ({ 
    display: 'flex',
    alignItems: 'center',
    position: 'relative',
    background: theme.palette.primary.main,
    color: '#4B4A4A',
    textTransform: 'uppercase',
    marginBottom: '40px'
}))

export const Background = styled('div')(({ theme}) => ({
    position: 'absolute',
    height: '100px',
    width: '100%',
    bottom: '0',
    textAlign: 'center',
    '&:before': {
        content: '""',
        display: 'block',
        position: 'absolute',
        borderRadius: '100% 50%',
        width: '55%',
        height: '100%',
        transform: 'translate(85%, 60%)',
        backgroundColor: '#FFFFFF'
    },
    '&:after': {
        content: '""',
        display: 'block',
        position: 'absolute',
        borderRadius: '100% 50%',
        width: '55%',
        height: '100%',
        backgroundColor: theme.palette.primary.main,
        transform: 'translate(-4%, 40%)',
        zIndex: '-1'
    }
}))

export const Content = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: `${theme.spacing(2)} ${theme.spacing(2)}`,
    '& > .MuiTypography-root': {
        marginLeft: theme.spacing(2)
    }
}))

export const Link = styled(RDLink)(({theme}) => ({
    width: theme.spacing(10),
    cursor: 'pointer',
    zIndex: 1000
}))

export const Logo = styled('img')(({theme}) => ({
    width: theme.spacing(10),
}))