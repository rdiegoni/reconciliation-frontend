import { MUITypography } from '@externals/mui';
import { CompanyLogoUrl } from '@utils/constants'
import * as styles from './Header.styles'

export const Header = () => {
    const { Root, Background, Link, Content, Logo } = styles
    return (
        <Root>
            <Background />    
            <Content>
                <Link to='/accounts'><Logo src={CompanyLogoUrl} /></Link>
                <MUITypography variant='h5'>Simplify your payment reconciliations</MUITypography>
            </Content>
        </Root>
    )
}