import { styled } from '@mui/system';

export const Root = styled('div')({
    display: 'inline-flex',
    alignItems: 'center'
})

export const Amount = styled('span')({})

export const Currency = styled('span')(({ theme }) => ({
    textTransform: 'uppercase',
    marginLeft: theme.spacing(1)
}));
    