import { formatNumber } from '@utils/number'
import * as styles from './Amount.styles'

interface AmountProps {
    amount: number
    currency?: string
}
    
export const Amount = (props: AmountProps) => {
    const { amount, currency } = props
    const { Root, Amount, Currency } = styles
    return (
        <Root>
            <Amount>{formatNumber(amount)}</Amount>
            <Currency>{currency}</Currency>
        </Root>
    )
}

Amount.defaultProps = {
    currency: 'GBP'
}