import { createTheme } from "@mui/material/styles";

export const theme = createTheme({
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        html: {
          "-webkit-font-smoothing": "antialiased",
          "-moz-osx-font-smoothing": "grayscale",
          height: "100%",
          width: "100%",
        },
        body: {
          margin: 0,
          height: "100%",
          width: "100%",
          overflowX: "hidden",
        },
        "*, *::before, *::after": {
          boxSizing: "inherit",
        },
        "#root": {
          height: "100%",
          width: "100%",
        },
      },
    },
  },
  palette: {
    primary: {
      main: "#19BEEB",
    },
    secondary: {
      main: "#009EB0",
    },
  },
});
