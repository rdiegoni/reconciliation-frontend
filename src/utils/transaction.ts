import { groupBy, sum, take } from "@externals/lodash";
import {
  Transaction,
  TransactionDto,
  TransactionType,
} from "@models/transaction";

export const convert = (data: TransactionDto): Transaction => {
  return {
    id: data.id,
    accountId: data.accountId,
    amount: data.amount,
    currency: data.currency,
    type: data.type as TransactionType,
    date: new Date(data.date),
  };
};

export const isSettled = (transaction: Transaction) =>
  transaction.type === "Settled";

export const getBalances = (transactions: Transaction[], index: number) => {
  const transactionsForCompute = take(transactions, index);
  const transactionsGrouped = groupBy(
    transactionsForCompute,
    (x) => x.currency
  );
  return Object.entries(transactionsGrouped).map(([key, value]) => ({
    currency: key,
    amount: sum(
      value.map(
        (transaction) => transaction.amount * (isSettled(transaction) ? 1 : -1)
      )
    ),
  }));
};
