const getBaseUrl = () => {
  return process.env.REACT_APP_API_BASE_URL;
};

export const getAccountsUrl = () => {
  return `${getBaseUrl()}/accounts`;
};

export const getAccountUrl = (accountId: string | undefined) => {
  return `${getBaseUrl()}/accounts/${accountId}`;
};

export const getTransactionsUrl = (accountId: string | undefined) => {
  return `${getBaseUrl()}/accounts/${accountId}/transactions`;
};
