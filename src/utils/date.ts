import { format } from "@externals/date-fns";

export const formatDate = (date: Date) => {
  return format(date, "dd/MM/yyyy HH:mm");
};
