import { Account, AccountDto } from "@models/account";

export const convert = (data: AccountDto): Account => {
  const { balances = [] } = data;
  return {
    id: data.id,
    name: data.name,
    balances: balances.map((x) => ({
      amount: x.amount,
      currency: x.currency,
    })),
  };
};
