# Reconciliation Frontend

The reconciliation frontend is a simple spa allowing the user to choose an account among the existing ones to see the list of transactions associated

## Prerequisite

- Node: >= v14.0.0

Use nvm to install that version of Node. It is much more easier to have multiple version of Node managed by cli

Once [nvm](https://github.com/nvm-sh/nvm) installed, run

- `nvm install v14.0.0` - to install the node version
- `nvm use v14.0.0` - to use the node version

## How to run it

### Locally

To run the spa locally, you have to execute the following commands

- `npm install`
- `npm start`

Open a browser with the url `http://localhost:3333` and check the spa is displayed

### Docker

To run the spa locally via docker, you have to execute the following commands

- `docker build -t reconciliation-spa:1.0.0 -f Dockerfile .`
- `docker run -ti --rm --name reconciliation-spa --publish 80:80 --detach reconciliation-spa:1.0.0`

## Architecture

### External libraries

For components, I have decided to use [Material UI](https://mui.com/material-ui/getting-started/installation/) which one of the famous Design System in the market.

For date, I have decided to use [date-fns](https://date-fns.org/) which the new date management library on the market. Moment being not maintained anymore.

For utils, I have decided to use [lodash](https://lodash.com/docs/) as it is the famous library currently on the market.

### Project structure

The project contains multiple directory

- `components` - contains an [atomic designs](https://dev.to/sanfra1407/how-to-organize-your-components-using-the-atomic-design-dj3) directory structure approach
- `externals` - contains the third libraries I used in that project. That approach allows to have a quick overview on how much I used them and it is easy to replace a part or the whole think without impacting too much the rest of the code.
- `hooks` - [new feature](https://reactjs.org/docs/hooks-intro.html) introduced in React 17 - I really enjoy to have on single feature in a hook such as getting the list of account. By developing with that approach everything is one place and it is really easy to plug it where you need it.
- `models` - list of the entity needed in the project. It allows me to know what kind of object I'm using and make the experience really easier.
- `pages` - contains the page of the project
- `theme` - define the theme that will be injected in the different components Material UI
- `utils` - list of util functions that make the code more readable and easy to maintain

### Language

To develop the spa, I decided to use

- `react` - it is currently still the best javascript framework on the market and I love the lego approach when using it.
- `typescript` - to make my developer experience better by getting the intellisense and get feedbacks on possible issues as soon as possible

### Maintainability

#### Directory structure/Naming convention

As part of our daily job, a developer has to maintain the project by fixing, refactoring, etc some piece of code.

Before starting the development, most of the time you have to identify the file and the line you have to introduce that new piece of code. Sometimes that step takes hours as the code is not easy to read. In order to get a time close to a few seconds, I notice the work starts from the directory structure and naming convention. That's why in my projects I decided to use the above directory structure and the following naming convention

- `[name].tsx` - file containing the representation of the component
- `[name].styles.tsx` - file containing the styled components needed to build the component
- others - the other files has a clear name allowing the developer to understand right away what it contains. For example, the file date.ts in utils directory let the developer knows it contains all utils method related to a date

I'm always trying to improve this part in each of my project and looking forward to find people who like to resolve these kind of problem as this is usually neglected

#### Typescript alias

Initially, when importing modules, the developer uses relative path. The problem with that approach is if you change the directory structure you have to updates all the impacted imports to reflect the new path. To resolve that problem and make a better developer experience, I used typescript alias. By doing so, you are minimizing the impact of changing directory structure.

For example, if you have an alias `alias1` targeting the path `directory1/directory2` which contains a complex sub structure, the impacts on your code can be different depending on what you are doing

- if you change the sub structure but the path `directory1/directory2` is unchanged, nothing will change in your project.
- if you change the path `directory1/directory2` to `directory1/directory3`, you just have to change the value of the alias in the `tsconfig.json` and nothing else

As you can see, we improved the developer experience to let him refactoring the way he want and minimizing the impact of it.

#### Code

Since I started developing with React, I really enjoy the lego approach. Each lego contains one single feature and you can easily plug/unplug it in your project. It is really easy to separate all concerns and compose to build what you expect. The current metaphor I'm using for that is going to a supermarket, taking the products I need for the week and pay.

Multiple benefits by keeping that in mind when developing

- minimize the number of line of code per file
- maximize the developer experience (reduce time to find the right file, easy to read, easy to change, etc)
- respect of the different principles (SOLID, KISS, DRY, etc)

#### Dto

In order to minimize the impacts of things your spa do not own, I try as much as possible to isolate these parts. For example, in that project I needed to communicate with the reconciliation backend in order to get the data that the spa will consume. Instead of creating a model that match the endpoints response and use it directly, I have followed the process

- Create a DTO that has the same signature as the endpoints' response
- Create a model that contains a signature matching my business expectation
- Create a converter that will convert the DTO to my model

By respecting that process, you have isolated what you do not own so any changes on the backend will be done in few seconds on the spa side.

## Improvements

### Code

Coding is like writing a book; it is subject to interpretation that's why it changes quite often. So, I'm sure we can find better naming, structure that can make a better difference on the developer experience

I would have love to use immutable structure for the data such as account, transaction etc to detect changes in JavaScript objects/arrays without resorting to the inefficiencies of deep equality checks, which in turn allows React to avoid expensive re-render operations when they are not required.

### Test

I would have loved set up the different tests

- `unit test` - test each feature in an isolated way - bring some confidence and help implementing better design. For example, I use to use jest + testing-library
- `visual testing` - a lot of teams use snapshot to test their UI which I'm not a fan with. The visual is exactly like your code. If you provide the same IN resulting in the same OUT you want your tests to succeed. Using snapshot testing does not do that. If you change your style producing the same OUT, your tests will failed. Usually, I use tools that generate images that will be compared to the previous generated ones. For example, I use to use storybook + chromatic.
- `integration testing` - test in a more wide way your project. For example, I use to use jest + testing-library
- `acceptance testing` - test your application in a user perspective. Usually representing by e2e tests. For example, I use to use jest + testing-library + cypress

### Deployment

I would have loved to deploy my solution in the cloud such as Heroku, AWS, etc in order to play with the whole project without having to get, build and run the projects

### Feature

I would have loved to add more features

- Add more visual to the user when there is no data or there are some errors during the experience
- Add some view options to display the transactions in different way such as per transaction type, per currency, etc
- Add some export options such as export data in csv
- Improve the user experience when loading data by displaying an indicator using React.suspense - Gold rule is to always display something to the user and not block the UI
- Improve performance of the spa by implementing a frontend micro service approach - for each page you load only the js/css you need for that page
- Monitor the spa by using [sentry](https://sentry.io/welcome/) - tool to track everything the user is doing with your application in order to investigate when issues occur
- Make the list of transactions responsive by using another component as the Table as it is not responsive
